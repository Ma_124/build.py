#!/usr/bin/python3

# MIT License
#
# Copyright (c) 2018 Ma_124
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import argparse
import os.path
from . import util
from .util import dynamic
from .util import builtins
from .util import io


class Language:
    extensions = None
    out_extension = None
    is_text = None
    line_comment = None

    def build(self, inp, out):
        # print(inp, '->', out)
        with open(inp, 'rb') as inp:
            with open(out, 'wb') as out:
                out.write(inp.read())

    def install(self):
        pass


class Config:
    OUT = -1
    ERR = -2
    COUT = -3
    CERR = -4

    default = 'build'
    out = 'out'
    log = [COUT]
    languages = []
    builtins = True
    preserve_paths = True

    def build(self):
        pass


cfg = Config()
langs = dict()
inst_dir = None
debug = False


def main(n, f=None):
    global cfg, langs, inst_dir, verbose, debug
    """
    build.main(__name__, __file__)

    :param n: __name__
    :param f: __file__
    """

    if n != '__main__':
        return
    inst_dir = os.path.dirname(__file__)
    par = argparse.ArgumentParser()
    par.add_argument('-f', '--file',   action='store', default='build',           help='The build configuration')
    par.add_argument('-o', '--out',    action='store',                            help='The output directory')
    par.add_argument('-l', '--log',    action='store',                            help='The log (see man page)')
    par.add_argument('-d', '--debug',  action='store_true',                       help='Print preprocessed source files')
    par.add_argument('task',           action='store',                 nargs='?', help='The task that should be called')
    par.add_argument('args',           action='store',                 nargs='*', help='The arguments for TASK')
    args = par.parse_args()

    if f is not None:
        args.file = f

    dynamic.load(args.file, cfg)

    if args.out is not None:
        cfg.out = args.out

    if args.log is not None:
        pass  # TODO parse arg

    debug = args.debug

    if args.task is None:
        args.task = cfg.default

    for lang in cfg.languages:
        l = Language()
        dynamic.load(os.path.join(inst_dir, 'lang', lang + '.py'), l, build_as_attr=True)
        for ext in l.extensions:
            langs[util.fmt_ext(ext)] = l

    io.mkdir(cfg.out)

    if dynamic.iscallable(cfg, args.task):
        builtins.pre(args.task, *args.args)
        getattr(cfg, args.task).__call__(cfg, *args.args)
    else:
        print("No task named " + args.task + "available.")


main(__name__)
