# MIT License
#
# Copyright (c) 2018 Ma_124
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import uuid
import atexit
import shutil


def walk(r):
    for root, dirs, files in os.walk(r):
        for dir in dirs:
            walk(os.path.join(root, dir))
        for file in files:
            yield os.path.join(root, file)


def mkdir(f, autodel=False):
    if not os.path.exists(f):
        os.mkdir(f, mode=0o770)
    if autodel:
        atexit.register(shutil.rmtree, f)


def tmp():
    return os.path.join('.tmp', uuid.uuid4().hex)


mkdir('.tmp', autodel=True)
