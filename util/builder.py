# MIT License
#
# Copyright (c) 2018 Ma_124
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os.path
from . import io
from . import preprocessor
from .logger import print
from .. import util
from .. import build as main


def out_name(inp, lang):
    n, ext = os.path.splitext(inp)
    if main.cfg.preserve_paths:
        return os.path.join(main.cfg.out, os.path.relpath(n) + util.fmt_ext(lang.out_extension))
    else:
        return os.path.join(main.cfg.out, os.path.basename(n) + util.fmt_ext(lang.out_extension))


def build(inp, out=None):
    n, ext = os.path.splitext(inp)
    if ext not in main.langs:
        return
    lang = main.langs[ext]
    if out is None:
        out = out_name(inp, lang)

    util.io.mkdir(os.path.dirname(out))

    tmp = util.io.tmp()
    print('Preprocessing ' + inp)
    util.preprocessor.process(lang, inp, tmp)
    # tmp2 = util.io.tmp()
    print('Building preprocessed ' + inp)
    lang.build(lang, tmp, out)
    # lang.build(lang, tmp, tmp2)
    # util.postprocessor.process(lang, tmp2, out)


def pre_build_task():
    for file in util.io.walk(os.getcwd()):
        if not os.path.commonpath([os.path.abspath('public'), os.path.abspath(file)]) == os.path.abspath('public'):
            build(file)
        else:
            print("Ignored " + file, lvl=2)

