# MIT License
#
# Copyright (c) 2018 Ma_124
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import importlib.util
from .. import build


def load(f, i, build_as_attr=False):
    spec = importlib.util.spec_from_file_location('module', f)
    mod = importlib.util.module_from_spec(spec)
    if build_as_attr:
        mod.build = build
    spec.loader.exec_module(mod)

    t = type(i)

    if hasattr(mod, t.__name__) and issubclass(getattr(mod, t.__name__), t):
        mod = getattr(mod, t.__name__)  # could return getattr(mod, t.__name__) but setting attrs in i is for the IDEs

    for attr in dir(mod):
        if not attr.startswith('_'):
            setattr(i, attr, getattr(mod, attr))


def iscallable(o, k):
    f = getattr(o, k, None)
    if f is None:
        return False
    if not hasattr(f, '__call__'):
        return False
    return True
