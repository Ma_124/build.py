# MIT License
#
# Copyright (c) 2018 Ma_124
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import re
from .logger import print
from .. import build


def _txt_process(lang, inp, out):
    with open(inp) as f:
        txt = f.read()
    if lang.line_comment is not None:
        txt = re.sub('((?<=[\n\r])|^)(\s*)' + re.escape(lang.line_comment + '&') + '(.*?)((?=[\n\r])|$)', _smpl_txt_repl, txt)
    with open(out, 'w') as f:
        f.write(txt)


def _smpl_txt_repl(m):
    globals()['indent'] = m.group(2)
    print('"' + m.group(2) + '"')
    gs = {
        'cfg': build.cfg,
        'include': include
    }
    for attr in dir(build.cfg):
        gs[attr] = getattr(build.cfg, attr)
    s = eval('str(' + m.group(3) + ')\n', gs)
    globals().pop('indent')
    return s


def process(lang, inp, out):
    # print(inp, '=>', out)
    if lang.is_text:
        _txt_process(lang, inp, out)
    else:
        with open(inp, 'rb') as inp:
            with open(out, 'wb') as out:
                out.write(inp.read())


def include(file, indent_aware=True):
    i = globals().get('indent', None)
    print('"' + i + '"')
    with open(file) as f:
        if not indent_aware or i is None or i == '':
            return f.read()
        ls = f.read()
    out = ''
    ls = ls.splitlines(True)
    for l in ls:
        out += i + l
    return out
