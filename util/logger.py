# MIT License
#
# Copyright (c) 2018 Ma_124
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import sys
import os
from .. import build


class Log:
    def write(self, inp):
        print(inp, nl=False, lvl=0)


log = Log()
#        exeout info   verbose
lvlcs = ['def', 'red', '']


def print(inp, nl=True, lvl=1):
    if nl:
        inp += os.linesep
    for L in build.cfg.log:
        if L == build.cfg.OUT:
            sys.stdout.write(inp)
        elif L == build.cfg.ERR:
            sys.stderr.write(inp)
        elif L == build.cfg.COUT:
            _cprint(inp, lvl=lvl)
        elif L == build.cfg.CERR:
            _cprint(inp, lvl=lvl, file=sys.stderr)
        # TODO file


_color = None


def _cprint(inp, lvl, file=sys.stdout):
    global _color
    if _color is None:  # Making termcolor package optional
        import termcolor as _color
    c = lvlcs[lvl]
    if c == 'def':
        file.write(inp)
    elif c == '':
        pass
    else:
        file.write(_color.colored(inp, color=c))

