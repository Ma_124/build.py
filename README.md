# build.py

Documentation: https://ma_124.gitlab.io/build.py/

build.py is a simple build system written in python.

Just create a `build.py` with the following content:

```python
#!/usr/bin/python3
import build

build.main(__name__)


class Config(build.Config):
  # Put your options here
  # Defaults:
  # Default task
  default = 'build'
  # Languages to build (lang/<lang>.py)
  languages = []
  # Default output directory
  out = 'out'
  # Whether to preserve output paths (src/dir/file.py -> out/dir/file.py or out/file.py)
  preserve_paths = True
  # Enable builtin tasks (build etc.)
  builtins = True

# Or remove the class above and put your options here
```

Then call `./build.py -h`  
usage: `build.py [-h] [-f FILE] [-o OUT] [task] [args [args ...]]`

***positional arguments***:

| Option   | Description                    |
|----------|--------------------------------|
| `task`   | The task that should be called |
| `args`   | The arguments for TASK         |

***optional arguments***:

| Short   | Long        | Description                     |
|-----------|---------------|---------------------------------|
| `-h`      | `--help`      | Show this help message and exit |
| `-f FILE` | `--file FILE` | The build configuration         |
| `-o OUT`  | `--out OUT`   | The output directory            |

## License

MIT License

Copyright (c) 2018 Ma_124

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
